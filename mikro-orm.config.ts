import { Book } from './src/entities/Book';
import { Author } from './src/entities/Author';
import 'dotenv/config'

export default {
  entities: [Author, Book],
  type: 'postgresql',
  dbName: process.env.DATABASE_NAME,
  user: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  seeder: {
    path: './src/seeders', // path to the folder with seeders
    defaultSeeder: 'DatabaseSeeder' // default seeder class name
}
};
