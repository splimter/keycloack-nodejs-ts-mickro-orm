import bodyParser from 'body-parser';
import express from 'express';
import { RequestContext } from '@mikro-orm/core';
import Database from './src/database';
import Router from './src/routes'
import keycloak from './src/keycloak-config';

const app = express();

app.use(bodyParser.json());
app.use(keycloak.initKeycloak().middleware());

app.listen(8000, async () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${8000}`);
  await Database();
  Router.mountRoutes(app);
});

// npx mikro-orm schema:update --run
