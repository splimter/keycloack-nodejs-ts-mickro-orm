import { Author } from './../entities/Author';
import { DB } from '../entities';

export const getAll = async (req: any, res: any) => {
    res.send({ authors: await DB.authorRepository.findAll() });
}

export const getById = async (req: any, res: any) => {
    const { id } = req.params;
    if (id) {
        res.send({ author: await DB.authorRepository.findOne(id) });
    } else {
        res.send({ error: 'missing id' });
    }
}


export const create = async (req: any, res: any) => {
    console.log(req.body);

    const { name, email } = req.body;
    if (name && email) {
        const author = new Author(name, email);

        try {
            await DB.authorRepository.persistAndFlush(author);
            res.send({ insertedId: author.id });
        } catch (error: any) {
            if (error.constraint === 'author_email_unique') {
                res.send({ error: 'email already in use' });
            }
        }

    } else {
        res.send({ error: 'missing atribute' });
    }
}

export const update = async (req: any, res: any) => {
    const { id } = req.params;
    console.log('update');
    if (id) {
        const author = await DB.authorRepository.findOne(req.params.id);
        if (!author) {
            return res.status(404).json({ error: 'entity not found' });
        } else {
            const { name, email } = req.body;
            author.name = name ?? author.email;
            author.email = email ?? author.email;
            try {
                await DB.authorRepository.persistAndFlush(author);
                res.send({ updatedId: author.id });
            } catch (error: any) {
                if (error.constraint === 'author_email_unique') {
                    res.send({ error: 'email already in use' });
                } else {
                    console.log(error);
                    
                }
            }

        }
    } else {
        res.send({ error: 'missing id' });
    }
}

export const remove = async (req: any, res: any) => {
    const { id } = req.params;
    if (id) {
        // await DB.authorRepository.removeAndFlush(id)
        await DB.orm.em.nativeDelete(Author, id);
        res.send({ removeId: id });
    } else {
        res.send({ error: 'missing id' });
    }
}