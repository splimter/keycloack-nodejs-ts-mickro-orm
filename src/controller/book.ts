import { Author } from './../entities/Author';
import { Book } from './../entities/Book';
import { DB } from '../entities';

export const getAll = async (req: any, res: any) => {
    res.send({ books: await DB.bookRepository.findAll() });
}

export const getById = async (req: any, res: any) => {
    const { id } = req.params;
    if (id) {
        res.send({ book: await DB.bookRepository.findOne(id) });
    } else {
        res.send({ error: 'missing id' });
    }
}


export const create = async (req: any, res: any) => {
    console.log(req.body);

    const { title, author } = req.body;
    if (title && author) {
        const _author = await DB.authorRepository.findOne(author);
        if (_author) {
            const book = new Book(title, _author);
            try {
                await DB.bookRepository.persistAndFlush(book);
                res.send({ insertedId: book.id });
            } catch (error: any) {
                if (error.constraint === 'book_email_unique') {
                    res.send({ error: 'email already in use' });
                }
            }
        } else {
            res.send({ error: 'no such author' });
        }
    } else {
        res.send({ error: 'missing atribute' });
    }
}

export const update = async (req: any, res: any) => {
    const { id } = req.params;
    console.log('update');
    if (id) {
        const book = await DB.bookRepository.findOne(req.params.id);
        if (!book) {
            return res.status(404).json({ error: 'entity not found' });
        } else {
            const { title, author } = req.body;
            const _author = await DB.authorRepository.findOne(author);
            book.title = title ?? book.title;
            book.author = _author ?? book.author;
            try {
                await DB.bookRepository.persistAndFlush(book);
                res.send({ updatedId: book.id });
            } catch (error: any) {
                if (error.constraint === 'book_email_unique') {
                    res.send({ error: 'email already in use' });
                } else {
                    console.log(error);

                }
            }

        }
    } else {
        res.send({ error: 'missing id' });
    }
}

export const remove = async (req: any, res: any) => {
    const { id } = req.params;
    if (id) {
        // await DB.bookRepository.removeAndFlush(id)
        await DB.orm.em.nativeDelete(Book, id);
        res.send({ removeId: id });
    } else {
        res.send({ error: 'missing id' });
    }
}