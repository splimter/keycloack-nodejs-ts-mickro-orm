export const get = (req: any, res: any) => {
    res.send({ get: 'get' });
}

export const post = (req: any, res: any) => {
    res.send({ post: 'post' });
}

export const put = (req: any, res: any) => {
    const { id } = req.params;
    res.send({ put: id });
}

export const remove = (req: any, res: any) => {
    const { id } = req.params;
    res.send({ remove: id });
}