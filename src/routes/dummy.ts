import { Router } from "express";
import { get, post, put, remove } from "../controller/dummy";

export default function dummyRoutes(app: Router) {
    app.get('/', get);
    
    app.post('/', post);
    
    app.put('/:id', put);
    
    app.delete('/:id', remove);
}

