import { Express, Router } from 'express'

import dummyRoutes from './dummy'
import authorRoutes from './author'
import bookRoutes from './book'

export default class MeshRoutes {
	public static mountRoutes(app: Express) {
		const dummyRouter = Router()
		dummyRoutes(dummyRouter)
        app.use('/', dummyRouter)

        const authorRouter = Router()
		authorRoutes(authorRouter)
        app.use('/author', authorRouter)

        const bookRouter = Router()
		bookRoutes(bookRouter)
        app.use('/book', bookRouter)
	}
}