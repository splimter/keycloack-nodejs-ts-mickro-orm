import { Router } from "express";
import { create, getAll, getById, update, remove } from "../controller/author";
import keycloak from '../keycloak-config';

export default function dummyRoutes(app: Router) {

    const kc = keycloak.getKeycloak();

    app.get('/', kc.protect('realm:user'), getAll);

    app.get('/:id', kc.protect('realm:user'), getById);

    app.post('/', kc.protect('realm:admin'), create);

    app.put('/:id', kc.protect('realm:admin'), update);

    app.delete('/:id', kc.protect('realm:admin'), remove);

}
