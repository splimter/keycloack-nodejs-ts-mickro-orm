import session from 'express-session';
import Keycloak from 'keycloak-connect';

let _keycloak: any;

var keycloakConfig: any = {
    "realm": "sciencewings-web",
    "auth-server-url": "http://localhost:8080/auth/",
    "ssl-required": "none",
    "resource": "sciencewings-web-client",
    "public-client": true,
    "confidential-port": 0
};

function initKeycloak() {
    if (_keycloak) {
        console.warn("Trying to init Keycloak again!");
        return _keycloak;
    }
    else {
        console.log("Initializing Keycloak...");
        var memoryStore = new session.MemoryStore();
        _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
        return _keycloak;
    }
}

function getKeycloak() {
    if (!_keycloak) {
        console.error('Keycloak has not been initialized. Please called init first.');
    }

    return _keycloak;
}

export default {
    initKeycloak,
    getKeycloak
};