
import { Book } from './entities/Book';
import { Author } from './entities/Author';
import { MikroORM } from "@mikro-orm/core";
import { DB } from './entities';
import 'dotenv/config'


export default async () => {
    DB.orm = await MikroORM.init({
        type: 'postgresql',
        host: 'localhost',
        allowGlobalContext: true,
        port: 5432,
        dbName: process.env.DATABASE_NAME,
        user: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        entities: [Author, Book],
    });
    DB.authorRepository = DB.orm.em.getRepository(Author);
    DB.bookRepository = DB.orm.em.getRepository(Book);
}