import { Factory } from '@mikro-orm/seeder';
import { faker } from '@faker-js/faker';
import { Author } from '../../entities/Author';

export class AuthorFactory extends Factory<Author> {
  model: any = Author;

  definition(f: typeof faker): Partial<Author> {
    return {
      name: f.name.findName(),
      email: f.internet.email(),
    };
  }
}