import { MikroORM } from '@mikro-orm/core';
import { EntityManager, EntityRepository } from "@mikro-orm/postgresql";
import { Author } from "./Author";
import { Book } from "./Book";

export const DB = {} as {
    orm: MikroORM,
    em: EntityManager,
    authorRepository: EntityRepository<Author>,
    bookRepository: EntityRepository<Book>,
  };
