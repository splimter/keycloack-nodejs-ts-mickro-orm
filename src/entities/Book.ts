import { BaseEntity, Collection, Entity, IdentifiedReference, ManyToMany, ManyToOne, PrimaryKey, Property, SerializedPrimaryKey } from "@mikro-orm/core";
import { Author } from './Author';

@Entity()
export class Book {

    @PrimaryKey()
    id!: number;

    @Property()
    title!: string;

    @ManyToOne()
    author!: Author;

    constructor(title: string, author: Author) {
        this.title = title;
        this.author = author;
    }

}