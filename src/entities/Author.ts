import { Collection, Entity, OneToMany, PrimaryKey, Property, Unique } from "@mikro-orm/core";
import { Book } from "./Book";

@Entity()
export class Author {

    @PrimaryKey()
    id!: number;

    @Property()
    createdAt: Date = new Date();

    @Property({ onUpdate: () => new Date() })
    updatedAt: Date = new Date()

    @Property()
    name!: string;

    @Property()
    @Unique()
    email!: string;

    @OneToMany('Book', 'author')
    books = new Collection<Book>(this);

    constructor(name: string, email: string) {
        this.name = name;
        this.email = email;
    }

}